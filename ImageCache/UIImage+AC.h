//
//  UIImage+AC.h
//  ImageCache
//
//  Created by Ankur on 02/09/15.
//  Copyright (c) 2015 Ankur Chauhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AC)

+ (void) loadFromURL: (NSString*) urlString cachePolicy:(BOOL) isCacheRequired callback:(void (^)(UIImage *image))callback;

+(void)clearACCacheFolder;


- (UIImage *)convertToGrayscale;

@end
