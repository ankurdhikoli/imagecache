//
//  UIImage+AC.m
//  ImageCache
//
//  Created by Ankur on 02/09/15.
//  Copyright (c) 2015 Ankur Chauhan. All rights reserved.
//

#import "UIImage+AC.h"

#define CacheFolderName @"ACCache"





@implementation UIImage (AC)


typedef enum {
    ALPHA = 0,
    BLUE = 1,
    GREEN = 2,
    RED = 3
} PIXELS;



#pragma Converting Grey Scale

- (UIImage *)convertToGrayscale {
    CGSize size = [self size];
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
            
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}



#pragma Lazy Loading


+ (void) loadFromURL: (NSString*) urlString cachePolicy:(BOOL) isCacheRequired callback:(void (^)(UIImage *image))callback {
    [[self class] createCacheFolder];
    NSString * imageName=nil;
    if (isCacheRequired) {
        imageName= [urlString lastPathComponent];
        UIImage *cachedImage=[[self class] getImageFromDocumentDirectoryWithName:imageName];
        
        if (cachedImage) {
            callback(cachedImage);
        }
    }
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImage *image = [UIImage imageWithData:imageData];
            if (isCacheRequired) {
                [[self class]saveImage:imageData:imageName];
            }
            callback(image);
        });
    });
}


+ (NSString *)createCacheFolder{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                        NSUserDomainMask,
                                                                        YES) lastObject];
    NSString *photoFolder = [documentsDirectory stringByAppendingPathComponent:CacheFolderName];
    
    // Create the folder if necessary
    BOOL isDir = NO;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if (![fileManager fileExistsAtPath:photoFolder
                           isDirectory:&isDir] && isDir == NO) {
        [fileManager createDirectoryAtPath:photoFolder
               withIntermediateDirectories:NO
                                attributes:nil
                                     error:nil];
    }
    return photoFolder;
}



+ (UIImage *)getImageFromDocumentDirectoryWithName :(NSString *)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory =    [documentsDirectory stringByAppendingPathComponent:CacheFolderName];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    return img;
}


+ (void)saveImage :(NSData *)imagetoSave :(NSString *)withName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:CacheFolderName];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:withName];

    [imagetoSave writeToFile:savedImagePath atomically:NO];
}

+(void)clearACCacheFolder{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    [documentsDirectory stringByAppendingPathComponent:CacheFolderName];

    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:documentsDirectory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }

}


@end
