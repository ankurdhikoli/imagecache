//
//  ViewController.m
//  ImageCache
//
//  Created by Ankur on 02/09/15.
//  Copyright (c) 2015 Ankur Chauhan. All rights reserved.
//

#import "ViewController.h"
#import "UIImage+AC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString *carstring=@"http://7-themes.com/data_images/out/63/6987796-mazda-sports-car.jpg";
   // NSString *carstring=@"https://images.omkt.co/Files/402/15EAC0/19F78/8c2112737cfe4cd88779712a4906ef6d/0/hero_forte_2014--kia-1920x.png";

    [UIImage loadFromURL:carstring cachePolicy:YES callback:^(UIImage *image) {
        if (image) {
            _imageVU.image=image;
//            [UIImage clearACCacheFolder];
        }
    }];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
